package com.example.appmovil_proyectomintic_grupo2_8.mvp;

import android.app.Activity;

public interface RegistroMVP {
    interface Model {
        void saveRegister(RegistroInfo user,
                          saveRegisterCallback callback);

        interface saveRegisterCallback {
            void onSuccess();

            void onFailure();
        }
    }

    interface Presenter {
        void onRegisterClick();
    }

    interface View {
        Activity getActivity();

        RegistroInfo getRegistroInfo();

        void showGeneralError(String error);

        void showEmailError(String error);

        void showPasswordError(String error);

        void showNameError(String error);

        void showAdressError(String error);

        void showPhoneError(String error);

        void showLoginActivity ();

        void showProgresBar();

        void hideProgresBar();


    }

    class RegistroInfo {

        private String name;
        private String adress;
        private String city;
        private String phone;
        private String email;
        private String password;
        private String Uid;

        public RegistroInfo(String name, String adress, String ciudad, String phone, String email, String password) {
            this.name = name;
            this.adress = adress;
            this.city = ciudad;
            this.phone = phone;
            this.email = email;
            this.password = password;
        }



        public String getName() {
            return name;
        }

        public String getAdress() {
            return adress;
        }

        public String getCity() {
            return city;
        }

        public String getPhone() {
            return phone;
        }

        public String getEmail() {
            return email;
        }

        public String getPassword() {
            return password;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getUid() {
            return Uid;
        }

        public void setUid(String uid) {
            this.Uid = Uid;
        }
    }
}

