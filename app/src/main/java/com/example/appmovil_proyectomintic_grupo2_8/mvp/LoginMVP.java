package com.example.appmovil_proyectomintic_grupo2_8.mvp;

import android.app.Activity;
import android.content.Intent;

public interface LoginMVP {
    interface Model {

        void validateCredentials(String email, String password,
                                 ValidateCredentialsCallback callback);

        boolean hasAuthenticatedUser();

        Intent getGoogleIntent();

        void setGoogleData(Intent data, ValidateCredentialsCallback Callback);

        interface ValidateCredentialsCallback {
            void onSuccess();

            void onFailure();
        }

    }

    interface Presenter {
        void onLoginClick();

        void onRegisterClick();

        void onGoogleClick();

        void validateHasUserAuthenticated();

        void setGoogleData(Intent data);
    }

    interface View {
        Activity getActivity();

        LoginInfo getLoginInfo();

        void showEmailError(String error);

        void showPasswordError(String error);

        void showRutasActivity ();

        void showGeneralError(String error);

        void showProgresBar();

        void hideProgresBar();

        void ShowRegistroActivity();

        void showGoogleSignInActivity(Intent intent);


    }

    class LoginInfo {
        private String email;
        private String password;

        public LoginInfo(String email, String password) {
            this.email = email;
            this.password = password;
        }

        public String getEmail() {
            return email;
        }

        public String getPassword() {
            return password;
        }
    }
}
