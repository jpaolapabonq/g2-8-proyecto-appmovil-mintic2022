package com.example.appmovil_proyectomintic_grupo2_8.mvp;

import android.app.Activity;
import android.content.Intent;

import java.util.List;

public interface OffersMVP {

           interface Model {

            void saveOffer(OfferInfo offer,
                           saveOfferCallback callback);
            interface saveOfferCallback {
                void onSuccess();

                void onFailure();
            }

        }


        interface Presenter {
            void onSaveOfferClick();
        }


        interface View {
            Activity getActivity();

            OfferInfo getOfferInfo();

            void showPerfilProfesionalActivity();

            void showGeneralError(String error);

            void showProgresBar();

            void hideProgresBar();

            void showGeneralMsg(String mensaje);

            void showNameError(String error);

            void showEspecialidadError(String error);

            void showDescripcionError(String error);

            void showLocationError(String error);

            void showCostoError(String error);
        }

        class OfferInfo {
            private String name;
            private String especialidad;
            private String descripcion;
            private String location;
            private String costo;

            public OfferInfo(String name, String especialidad, String descripcion, String location, String costo) {
                this.name = name;
                this.especialidad = especialidad;
                this.descripcion = descripcion;
                this.location = location;
                this.costo = costo;
            }

            public String getName() {
                return name;
            }

            public String getEspecialidad() {
                return especialidad;
            }

            public String getDescripcion() {
                return descripcion;
            }

            public String getLocation() {
                return location;
            }

            public String getCosto() {
                return costo;
            }
        }

    }

