package com.example.appmovil_proyectomintic_grupo2_8.mvp;

import android.app.Activity;
import android.os.Bundle;

import java.util.List;

public interface ProfesionalesMVP {

    interface Model {
        void loadProfesionales(LoadProfesionalesCallback callback);

        interface LoadProfesionalesCallback {
            void setProfesionales(List<ProfesionalDto> profesionales);

        }
    }

    interface Presenter {

        void loadProfesionales();

        void onProfesionalClick();

        void onLlamarClick();

        void onCotizacionClick();

        void onLocalizacion();

        void onItemSelected(ProfesionalDto info);
    }

    interface View {
        Activity getActivity();

        void showProgressBar();

        void hideProgressBar();

        void showProfesioanles(List<ProfesionalDto> profesionales);

        void openLocationActivity(Bundle params);
    }

    class ProfesionalDto {

        private String nameprofesional;
        private String contactoprofesional;
        private String emailprofesional;
        private String locationprofesional;
        private String calificacionprofesional;
        private String costoprofesional;

        public ProfesionalDto(String nameprofesional, String contactoprofesional, String emailprofesional, String locationprofesional, String calificacionprofesional, String costoprofesional) {
            this.nameprofesional = nameprofesional;
            this.contactoprofesional = contactoprofesional;
            this.emailprofesional = emailprofesional;
            this.locationprofesional = locationprofesional;
            this.calificacionprofesional = calificacionprofesional;
            this.costoprofesional = costoprofesional;
        }

        public String getNameprofesional() {
            return nameprofesional;
        }

        public String getContactoprofesional() {
            return contactoprofesional;
        }

        public String getEmailprofesional() {
            return emailprofesional;
        }

        public String getLocationprofesional() {
            return locationprofesional;
        }

        public String getCalificacionprofesional() {
            return calificacionprofesional;
        }

        public String getCostoprofesional() {
            return costoprofesional;
        }
    }
}
