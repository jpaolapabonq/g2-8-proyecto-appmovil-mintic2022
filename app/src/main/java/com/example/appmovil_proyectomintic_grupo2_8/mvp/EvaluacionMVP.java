package com.example.appmovil_proyectomintic_grupo2_8.mvp;

public interface EvaluacionMVP {

    interface Model {

    }

    interface Presenter {
        void onEnviarCalificacionClick();

        void onRegisterClick();
    }

    interface View {

    }
}


