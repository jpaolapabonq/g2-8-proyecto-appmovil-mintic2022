package com.example.appmovil_proyectomintic_grupo2_8.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;

import com.example.appmovil_proyectomintic_grupo2_8.R;
import com.example.appmovil_proyectomintic_grupo2_8.model.repository.FirebaseAuthRepository;
import com.example.appmovil_proyectomintic_grupo2_8.mvp.RegistroMVP;
import com.example.appmovil_proyectomintic_grupo2_8.presenter.RegistroPresenter;
import com.google.android.material.progressindicator.LinearProgressIndicator;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseUser;

public class RegistroActivity extends AppCompatActivity implements RegistroMVP.View{
    // Campos formulario registro

    private LinearProgressIndicator pbWait;
    private TextInputLayout tilName;
    private TextInputEditText etName;
    private TextInputLayout tilDireccion;
    private TextInputEditText etDireccion;
    private TextInputLayout tilCiudad;

    private TextInputLayout tilNumeroTelefono;
    private TextInputEditText etNumeroTelefono;
    private TextInputLayout tilemail;
    private TextInputEditText etemail;
    private TextInputLayout tilpassword;
    private TextInputEditText etpassword;

    private String [] city = {"Armenia","Choco", "Manizales", "Pasto","Popayan"};
    private AutoCompleteTextView autocompletecity;
    private ArrayAdapter<String > adaptercity;

    private AppCompatButton btnRegister;

    private RegistroMVP.Presenter presenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        presenter = new RegistroPresenter(RegistroActivity.this);

        initUI();
    }


    private void initUI() {

        pbWait = findViewById(R.id.pb_wait);

        tilName = findViewById(R.id.til_Name);
        etName = findViewById(R.id.et_Name);
        tilDireccion = findViewById(R.id.til_adress);
        etDireccion = findViewById(R.id.et_adress);
        tilCiudad = findViewById(R.id.til_city);

        tilNumeroTelefono = findViewById(R.id.til_phone);
        etNumeroTelefono = findViewById(R.id.et_phone);
        tilemail = findViewById(R.id.til_email);
        etemail = findViewById(R.id.et_email);
        tilpassword = findViewById(R.id.til_password);
        etpassword = findViewById(R.id.et_password);

        btnRegister = findViewById(R.id.btn_register);
        btnRegister.setOnClickListener(v -> presenter.onRegisterClick());

        autocompletecity = findViewById(R.id.autocomplete_city);
        adaptercity = new ArrayAdapter<String>(this, R.layout.list_item, city);

        autocompletecity.setAdapter(adaptercity);

        autocompletecity.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
                Toast.makeText(getApplicationContext(), "Categoria:" + item, Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public Activity getActivity() {
        return RegistroActivity.this;
    }

    @Override
    public RegistroMVP.RegistroInfo getRegistroInfo() {
        return new RegistroMVP.RegistroInfo(
                etName.getText().toString().trim(),
                etDireccion.getText().toString().trim(),
                autocompletecity.getText().toString().trim(),
                etNumeroTelefono.getText().toString().trim(),
                etemail.getText().toString().trim(),
                etpassword.getText().toString().trim());
    }

    @Override
    public void showGeneralError(String error) {
        Toast.makeText(RegistroActivity.this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showEmailError(String error) {
        tilemail.setError(error);
    }

    @Override
    public void showPasswordError(String error) {
        tilpassword.setError(error);
    }

    @Override
    public void showNameError(String error) {
        tilName.setError(error);
    }

    @Override
    public void showAdressError(String error) {
        tilDireccion.setError(error);
    }

    @Override
    public void showPhoneError(String error) {
        tilNumeroTelefono.setError(error);
    }

    @Override
    public void showLoginActivity() {
        Intent intent = new Intent(RegistroActivity.this, LoginActivity.class);
        startActivity(intent);


    }

    @Override
    public void showProgresBar() {
        pbWait.setVisibility(View.VISIBLE);
        btnRegister.setEnabled(false);
    }

    @Override
    public void hideProgresBar() {
        pbWait.setVisibility(View.GONE);
        btnRegister.setEnabled(true);
    }



}
