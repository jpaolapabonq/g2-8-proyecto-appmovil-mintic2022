package com.example.appmovil_proyectomintic_grupo2_8.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import com.example.appmovil_proyectomintic_grupo2_8.R;

public class MainActivity extends AppCompatActivity {

    private Button btnSignIn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initUI();
        }
    private void initUI() {


        btnSignIn = findViewById (R.id.btn_sign_in);
        btnSignIn.setOnClickListener(v ->onSigninClick());

        }


    private void onSigninClick() {
        Intent intent = new Intent(MainActivity.this, LoginActivity.class );
        startActivity(intent);
    }


}