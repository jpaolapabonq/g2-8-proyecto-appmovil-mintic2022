package com.example.appmovil_proyectomintic_grupo2_8.view;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.appmovil_proyectomintic_grupo2_8.R;
import com.example.appmovil_proyectomintic_grupo2_8.mvp.LoginMVP;
import com.example.appmovil_proyectomintic_grupo2_8.presenter.LoginPresenter;
import com.google.android.gms.common.SignInButton;
import com.google.android.material.progressindicator.LinearProgressIndicator;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

public class LoginActivity extends AppCompatActivity implements LoginMVP.View {

    private LinearProgressIndicator pbWait;
    private TextInputLayout tilEmail;
    private TextInputEditText etEmail;
    private TextInputLayout tilPassword;
    private TextInputEditText etPassword;
    private AppCompatButton btnLogin;
    private AppCompatButton btnRegister;
    private SignInButton btnGoogle;

    private LoginMVP.Presenter presenter;

    // atributos para autenticar en google
    private final int RC_SIGN_IN = 1;
    private ActivityResultLauncher<Intent> googleLauncher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        presenter = new LoginPresenter(LoginActivity.this);
        presenter.validateHasUserAuthenticated();

        initUI();
    }

    private void initUI() {
        pbWait = findViewById(R.id.pb_wait);

        tilEmail = findViewById(R.id.til_email);
        etEmail = findViewById(R.id.et_email);

        tilPassword = findViewById(R.id.til_password);
        etPassword = findViewById(R.id.et_password);

        btnLogin = findViewById(R.id.btn_inicio);
        btnLogin.setOnClickListener(v -> presenter.onLoginClick());

        btnRegister = findViewById(R.id.btn_register);
        btnRegister.setOnClickListener(v -> presenter.onRegisterClick());

        // boton de google
        btnGoogle = findViewById(R.id.btn_google);
        btnGoogle.setOnClickListener(v -> presenter.onGoogleClick());
        googleLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        // There are no request codes
                        Intent data = result.getData();
                        presenter.setGoogleData(data);
                    } else {
                        hideProgresBar();
                    }
                });


    }

    @Override
    public Activity getActivity() {
        return LoginActivity.this;
    }

    @Override
    public LoginMVP.LoginInfo getLoginInfo() {
        return new LoginMVP.LoginInfo(
                etEmail.getText().toString().trim(),
                etPassword.getText().toString().trim());
    }

    @Override
    public void showEmailError(String error) {
        tilEmail.setError(error);
    }

    @Override
    public void showPasswordError(String error) {
        tilPassword.setError(error);
    }

    @Override
    public void showRutasActivity () {
        Intent intent = new Intent(LoginActivity.this, RutasActivity.class);
        startActivity(intent);
    }

    @Override
    public void showGeneralError(String error) {
        Toast.makeText(LoginActivity.this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgresBar() {
        pbWait.setVisibility(View.VISIBLE);
        btnLogin.setEnabled(false);
        btnRegister.setEnabled(false);

    }

    @Override
    public void hideProgresBar() {
        pbWait.setVisibility(View.GONE);
        btnLogin.setEnabled(true);
        btnRegister.setEnabled(true);

    }

    @Override
    public void ShowRegistroActivity() {
        Intent intent = new Intent(LoginActivity.this, RegistroActivity.class);
        startActivity(intent);

    }

    @Override
    public void showGoogleSignInActivity(Intent intent) {
        googleLauncher.launch(intent);
    }

}
