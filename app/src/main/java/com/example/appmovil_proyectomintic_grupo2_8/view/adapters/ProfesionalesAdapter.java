package com.example.appmovil_proyectomintic_grupo2_8.view.adapters;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.appmovil_proyectomintic_grupo2_8.R;
import com.example.appmovil_proyectomintic_grupo2_8.mvp.ProfesionalesMVP;

import java.util.ArrayList;
import java.util.List;

public class ProfesionalesAdapter extends  RecyclerView.Adapter<ProfesionalesAdapter.ViewHolder>{

    private List<ProfesionalesMVP.ProfesionalDto> data;
    private OnItemClickListener onItemClickListener;

    public ProfesionalesAdapter() {
        this.data = new ArrayList<>();
        }
    public void setData(List<ProfesionalesMVP.ProfesionalDto> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_profesionales, parent, false);

        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ProfesionalesMVP.ProfesionalDto item = data.get(position);

        // Asignar click listener a elemento
        if (onItemClickListener != null) {
            holder.itemView.setOnClickListener(v -> onItemClickListener.onItemClick(item));
        }

        holder.getName().setText(data.get(position).getNameprofesional());
        holder.getContacto().setText(data.get(position).getContactoprofesional());
        holder.getEmail().setText(data.get(position).getEmailprofesional());
        holder.getLocation().setText(data.get(position).getLocationprofesional());
        holder.getCalificacion().setText(data.get(position).getCalificacionprofesional());
        holder.getCosto().setText(data.get(position).getCostoprofesional());

    }

    //Asignar click listener a elemento
    // PENDIENTE

    @Override
    public int getItemCount() {
        return data.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageprofesional;
        private TextView name;
        private TextView contacto;
        private TextView email;
        private TextView location;
        private TextView calificacion;
        private TextView costo;

        public ViewHolder(View view) {
            super(view);

            initUI(view);
        }

        private void initUI(View view) {
            imageprofesional = view.findViewById(R.id.iv_imageprofesional);
            name = view.findViewById(R.id.name_profesional);
            contacto= view.findViewById(R.id.contacto);
            email= view.findViewById(R.id.email);
            location= view.findViewById(R.id.location);
            calificacion= view.findViewById(R.id.calificacion);
            costo= view.findViewById(R.id.costo);

        }

        public ImageView getImageprofesional() {
            return imageprofesional;
        }

        public TextView getName() {
            return name;
        }

        public TextView getContacto() {
            return contacto;
        }

        public TextView getEmail() {
            return email;
        }

        public TextView getLocation() {
            return location;
        }

        public TextView getCalificacion() {
            return calificacion;
        }

        public TextView getCosto() {
            return costo;
        }
    }

    public interface OnItemClickListener {
        void onItemClick(ProfesionalesMVP.ProfesionalDto info);
    }
}
