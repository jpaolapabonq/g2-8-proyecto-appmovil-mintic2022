package com.example.appmovil_proyectomintic_grupo2_8.view;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.content.Intent;
import android.os.Bundle;

import com.example.appmovil_proyectomintic_grupo2_8.R;
import com.example.appmovil_proyectomintic_grupo2_8.model.repository.FirebaseAuthRepository;

public class RutasActivity extends AppCompatActivity {

    private AppCompatButton btnSolicitar;
    private AppCompatButton btnOfrecer;
    private AppCompatButton btnCalificar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rutas);
        initUI();
    }
    private void initUI() {

        btnSolicitar = findViewById(R.id.btn_solicitar);
        btnSolicitar.setOnClickListener(v -> onSolicitarClick());

        btnOfrecer = findViewById(R.id.btn_ofrecer);
        btnOfrecer.setOnClickListener(v -> onOfertaClick());

        btnCalificar = findViewById(R.id.btn_calificar);
        btnCalificar.setOnClickListener(v -> onCalificarClick());


    }
    private void onSolicitarClick() {
        // TODO Pendiente de implementar
        Intent intent = new Intent(RutasActivity.this, RequestclientActivity.class);
        startActivity(intent);
    }
    private void onOfertaClick() {
        // TODO Pendiente de implementar
        Intent intent = new Intent(RutasActivity.this, ServiceofferActivity.class);
        startActivity(intent);
    }
    private void onCalificarClick() {
        Intent intent = new Intent(RutasActivity.this, EvaluaciontrabajadorActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {


        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle(R.string.app_name)
                .setMessage("Seguro quieres cerrar la sesión?")
                .setPositiveButton("Ok",
                        (dialog, which) -> {
                            FirebaseAuthRepository repository = FirebaseAuthRepository.getInstance(RutasActivity.this);
                            repository.logOut();

                            RutasActivity.super.onBackPressed();
                        })
                .setNegativeButton("Cancel", null);

        builder.create().show();
    }
}


