package com.example.appmovil_proyectomintic_grupo2_8.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.example.appmovil_proyectomintic_grupo2_8.R;
import com.example.appmovil_proyectomintic_grupo2_8.mvp.EvaluacionMVP;
import com.example.appmovil_proyectomintic_grupo2_8.presenter.EvaluacionPresenter;

public class EvaluaciontrabajadorActivity extends AppCompatActivity implements EvaluacionMVP.View {

    Spinner PuntualitySpinner;
    Spinner CommunicationSpinner;
    Spinner KnowledgeSpinner;
    Spinner AgilitySpinner;
    Spinner QualitySpinner;

    private Spinner puntualitySpinner;
    private Spinner communicationSpinner;
    private Spinner knowledgeSpinner;
    private Spinner agilitySpinner;
    private Spinner qualitySpinner;

    private Button btnenviarcalificacion;

    private EvaluacionMVP.Presenter presenter;

    //options to be displayed
    String[] Options = {"1","2","3","4","5"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evaluaciontrabajador);

        PuntualitySpinner = findViewById(R.id.PuntualitySpinner);
        //Creating the ArrayAdapter instance having the list of options
        ArrayAdapter option_puntuality = new ArrayAdapter(this, android.R.layout.simple_spinner_item, Options);
        option_puntuality.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //setting the ArrayAdapter data on the Spinner
        PuntualitySpinner.setAdapter(option_puntuality);

        CommunicationSpinner = findViewById(R.id.CommunicationSpinner);
        //Creating the ArrayAdapter instance having the list of options
        ArrayAdapter option_communication = new ArrayAdapter(this, android.R.layout.simple_spinner_item, Options);
        option_communication.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //setting the ArrayAdapter data on the Spinner
        CommunicationSpinner.setAdapter(option_communication);

        KnowledgeSpinner = findViewById(R.id.KnowledgeSpinner);
        //Creating the ArrayAdapter instance having the list of options
        ArrayAdapter option_knowledge = new ArrayAdapter(this, android.R.layout.simple_spinner_item, Options);
        option_communication.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //setting the ArrayAdapter data on the Spinner
        KnowledgeSpinner.setAdapter(option_communication);

        AgilitySpinner = findViewById(R.id.AgilitySpinner);
        //Creating the ArrayAdapter instance having the list of options
        ArrayAdapter option_agility = new ArrayAdapter(this, android.R.layout.simple_spinner_item, Options);
        option_communication.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //setting the ArrayAdapter data on the Spinner
        AgilitySpinner.setAdapter(option_communication);

        QualitySpinner = findViewById(R.id.QualitySpinner);
        //Creating the ArrayAdapter instance having the list of options
        ArrayAdapter option_quality = new ArrayAdapter(this, android.R.layout.simple_spinner_item, Options);
        option_communication.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //setting the ArrayAdapter data on the Spinner
        QualitySpinner.setAdapter(option_communication);

        presenter = new EvaluacionPresenter(EvaluaciontrabajadorActivity.this);

        initUI();
    }
    private void initUI() {

        puntualitySpinner = findViewById(R.id.PuntualitySpinner);
        communicationSpinner = findViewById(R.id.CommunicationSpinner);
        knowledgeSpinner = findViewById(R.id.KnowledgeSpinner);
        agilitySpinner = findViewById(R.id.AgilitySpinner);
        qualitySpinner = findViewById(R.id.QualitySpinner);

        btnenviarcalificacion = findViewById (R.id.btn_enviarcalificacion);
        btnenviarcalificacion.setOnClickListener(v ->onEnviarCalificacionClick());
    }
    private void onEnviarCalificacionClick() {
        Intent intent = new Intent(EvaluaciontrabajadorActivity.this, LoginActivity.class );
        startActivity(intent);
    }
}
