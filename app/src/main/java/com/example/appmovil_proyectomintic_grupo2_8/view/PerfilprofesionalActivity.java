package com.example.appmovil_proyectomintic_grupo2_8.view;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.example.appmovil_proyectomintic_grupo2_8.R;
import com.example.appmovil_proyectomintic_grupo2_8.model.repository.FirebaseAuthRepository;
import com.google.firebase.auth.FirebaseUser;

public class PerfilprofesionalActivity extends AppCompatActivity {

    private TextView tvnameprofesional;
    private TextView tvemail;
    private TextView tvespecialidad;
    private TextView tvdescripcion;
    private TextView tvlocation;
    private TextView tvcosto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfilprofesional);

        initUI();
        loadData();
        }

    private void initUI() {
        tvnameprofesional = findViewById(R.id.tv_name_perfil);
        tvemail = findViewById(R.id.tv_email);
        tvespecialidad = findViewById(R.id.tv_especialidad);
        tvdescripcion = findViewById(R.id.tv_descripcion);
        tvlocation = findViewById(R.id.tv_location);
        tvcosto= findViewById(R.id.tv_costo);
    }

    private void loadData() {
        FirebaseAuthRepository repository = FirebaseAuthRepository.getInstance(this);
        FirebaseUser user = repository.getCurrentUser();
        tvemail.setText(user.getEmail());
    }

}
