package com.example.appmovil_proyectomintic_grupo2_8.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.appmovil_proyectomintic_grupo2_8.R;
import com.example.appmovil_proyectomintic_grupo2_8.mvp.ProfesionalesMVP;
import com.example.appmovil_proyectomintic_grupo2_8.presenter.ProfesionalesPresenter;
import com.example.appmovil_proyectomintic_grupo2_8.view.adapters.ProfesionalesAdapter;
import com.google.android.material.progressindicator.CircularProgressIndicator;
import com.google.android.material.progressindicator.LinearProgressIndicator;

import java.util.List;

public class ProfesionalesActivity extends AppCompatActivity implements ProfesionalesMVP.View {

    private LinearProgressIndicator pbwait;

    private RecyclerView rvListaProfesionales;
    private TextView calificar;
    private ImageButton btnLlamar;
    private ImageButton btnCotizar;
    private ImageButton btnLocalizacion;

    private ProfesionalesMVP.Presenter presenter;
    private ProfesionalesAdapter profesionalesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profesionales);

        presenter = new ProfesionalesPresenter(ProfesionalesActivity.this);


        initUI();
        presenter.loadProfesionales();
    }

    private void initUI () {

        pbwait = findViewById(R.id.pb_wait);

        calificar = findViewById(R.id.tv_subcategoria_title);
        calificar.setOnClickListener(v -> onCalificarClick());

        profesionalesAdapter = new ProfesionalesAdapter();
        profesionalesAdapter.setOnItemClickListener(info -> presenter.onItemSelected(info));

        rvListaProfesionales = findViewById(R.id.rv_profesionales);
        rvListaProfesionales.setLayoutManager(new LinearLayoutManager(ProfesionalesActivity.this));
        rvListaProfesionales.setAdapter(profesionalesAdapter);

    }
    private void onCalificarClick() {
        Intent intent = new Intent(ProfesionalesActivity.this, EvaluaciontrabajadorActivity.class);
        startActivity(intent);
    }

    @Override
    public Activity getActivity() {
        return ProfesionalesActivity.this;
    }

    @Override
    public void showProgressBar() {
        pbwait.setVisibility(View.VISIBLE);

    }

    @Override
    public void hideProgressBar() {
        pbwait.setVisibility(View.GONE);

    }

    @Override
    public void showProfesioanles(List<ProfesionalesMVP.ProfesionalDto> profesionales) {

        profesionalesAdapter.setData(profesionales);
    }

    @Override
    public void openLocationActivity(Bundle params) {
        Intent intent = new Intent(ProfesionalesActivity.this, LocationActivity.class);
        intent.putExtras(params);
        startActivity(intent);
    }
}
