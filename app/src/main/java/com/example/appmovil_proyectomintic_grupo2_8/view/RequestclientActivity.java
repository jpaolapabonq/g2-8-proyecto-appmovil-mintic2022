package com.example.appmovil_proyectomintic_grupo2_8.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;

import com.example.appmovil_proyectomintic_grupo2_8.R;
import com.google.android.material.datepicker.CalendarConstraints;
import com.google.android.material.datepicker.DateValidatorPointForward;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class RequestclientActivity extends AppCompatActivity {

    private AppCompatButton btnEnviarsolisitud;

    private String [] categorias = {"ALBAÑIL", "PINTOR", "CARPINTERO","JARDINERO","LIMPIEZA","CERRAJERO"};
    private AutoCompleteTextView autocompletecategoria;
    private ArrayAdapter<String > adaptercategoria;

    private String [] subcategorias = {"Paisajismo", "Mantenimiento", "Poda en altura"};
    private AutoCompleteTextView autocompletesubcategoria;
    private ArrayAdapter<String > adaptersubcategoria;

    private TextInputLayout tilDate;
    private Date selectedDate;
    private TextInputEditText etDate;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_requestclient);

    initUI();
    }
    private void initUI() {

        btnEnviarsolisitud = findViewById(R.id.btn_send);
        btnEnviarsolisitud.setOnClickListener(v -> onSendRequestClick());

        tilDate = findViewById(R.id.request_til_date);
        tilDate.setEndIconOnClickListener(v -> onDateClick());

        etDate = findViewById(R.id.tet_date);

        autocompletecategoria = findViewById(R.id.autocomplete_categoria);
        adaptercategoria = new ArrayAdapter<String>(this, R.layout.list_item, categorias);

        autocompletecategoria.setAdapter(adaptercategoria);

        autocompletecategoria.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
                Toast.makeText(getApplicationContext(), "Categoria:" + item, Toast.LENGTH_SHORT).show();
            }
        });

        autocompletesubcategoria = findViewById(R.id.autocomplete_subcategoria);
        adaptersubcategoria = new ArrayAdapter<String>(this, R.layout.list_item, subcategorias);

        autocompletesubcategoria.setAdapter(adaptersubcategoria);

        autocompletesubcategoria.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
                Toast.makeText(getApplicationContext(), "Subcategoria:" + item, Toast.LENGTH_SHORT).show();
            }
        });

    }
    private void onDateClick() {
        long today = MaterialDatePicker.todayInUtcMilliseconds();
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        calendar.setTimeInMillis(today);
        calendar.add(Calendar.DAY_OF_MONTH, 1);

        CalendarConstraints constraint = new CalendarConstraints.Builder()
                .setValidator(DateValidatorPointForward.from(calendar.getTimeInMillis()))
                .build();

        MaterialDatePicker<Long> datePicker = MaterialDatePicker.Builder.datePicker()
                .setTitleText(R.string.service_date)
                .setSelection(calendar.getTimeInMillis())
                .setCalendarConstraints(constraint)
                .build();
        datePicker.addOnPositiveButtonClickListener(this::setSelectedDate);

        datePicker.show(getSupportFragmentManager(), "date");
    }

    private void setSelectedDate(Long selection) {
        selectedDate = new Date(selection);
        etDate.setText(SimpleDateFormat.getDateInstance().format(selectedDate));
    }

    private void onSendRequestClick() {
        // TODO Pendiente de implementar
        Intent intent = new Intent(RequestclientActivity.this, ProfesionalesActivity.class);
        startActivity(intent);
    }
    }