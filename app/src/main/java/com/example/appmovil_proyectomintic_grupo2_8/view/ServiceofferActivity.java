package com.example.appmovil_proyectomintic_grupo2_8.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.leanback.widget.Presenter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;

import com.example.appmovil_proyectomintic_grupo2_8.R;
import com.example.appmovil_proyectomintic_grupo2_8.model.repository.FirebaseAuthRepository;
import com.example.appmovil_proyectomintic_grupo2_8.mvp.OffersMVP;
import com.example.appmovil_proyectomintic_grupo2_8.presenter.OffersPresenter;
import com.google.android.material.progressindicator.LinearProgressIndicator;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class ServiceofferActivity extends AppCompatActivity implements OffersMVP.View {

    // base de datos
    DatabaseReference offerReference;

    private AppCompatButton btnoffer;

    private LinearProgressIndicator pbWait;

    private TextInputLayout tilname;
    private TextInputEditText tetname;

    private TextInputLayout tilespecialidad;
    private String [] especialidad = {"ALBAÑIL","CARPINTERO","JARDINERO","LIMPIEZA","CERRAJERO"};
    private AutoCompleteTextView acespecialidad;
    private ArrayAdapter<String > adapterespecialidad;

    private TextInputLayout tildescripcion;
    private TextInputEditText tetdescripcion;

    private TextInputLayout tillocaltion;
    private TextInputEditText tetlocation;

    private TextInputLayout tilcosto;
    private TextInputEditText tetcosto;

    private AppCompatButton btnEnviaroferta;

    // pendiente implementar en oferta de servicios
    private TextInputLayout tilDate;
    private Date selectedDate;
    private TextInputEditText etDate;

    private OffersMVP.Presenter presenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_serviceoffer);

        presenter = new OffersPresenter(ServiceofferActivity.this);

        initUI ();
        loadData();
    }

    private void initUI() {
        pbWait = findViewById(R.id.pb_wait);

        tilname = findViewById(R.id.til_name);
        tetname = findViewById(R.id.tet_name);


        tilespecialidad = findViewById(R.id.til_especialidad);
        acespecialidad = findViewById(R.id.auc_especialidad);
        adapterespecialidad = new ArrayAdapter<String>(this, R.layout.list_item, especialidad);

        acespecialidad.setAdapter(adapterespecialidad);

        acespecialidad.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
                Toast.makeText(getApplicationContext(), "Categoria:" + item, Toast.LENGTH_SHORT).show();
            }
        });

        tildescripcion = findViewById(R.id.til_description);
        tetdescripcion = findViewById(R.id.tet_description);

        tillocaltion = findViewById(R.id.til_location);
        tetlocation = findViewById(R.id.tet_location);

        tilcosto = findViewById(R.id.til_valor);
        tetcosto = findViewById(R.id.tet_valor);

        btnEnviaroferta= findViewById(R.id.btn_sendoffer1);
        btnEnviaroferta.setOnClickListener(v -> presenter.onSaveOfferClick());

    }

    private void loadData() {
        FirebaseAuthRepository repository = FirebaseAuthRepository.getInstance(this);
        FirebaseUser user = repository.getCurrentUser();
        tetname.setText(user.getEmail());
    }

    @Override
    public Activity getActivity() {
        return ServiceofferActivity.this;
    }

    @Override
    public OffersMVP.OfferInfo getOfferInfo() {
        return new OffersMVP.OfferInfo(
                tetname.getText().toString().trim(),
                acespecialidad.getText().toString().trim(),
                tetdescripcion.getText().toString().trim(),
                tetlocation.getText().toString().trim(),
                tetcosto.getText().toString().trim()
        );

    }

    @Override
    public void showPerfilProfesionalActivity() {
        Intent intent = new Intent(ServiceofferActivity.this, PerfilprofesionalActivity.class);
        startActivity(intent);
    }

    @Override
    public void showGeneralError(String error) {
        Toast.makeText(ServiceofferActivity.this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgresBar() {

    }

    @Override
    public void hideProgresBar() {

    }

    @Override
    public void showGeneralMsg(String mensaje) {
        Toast.makeText(ServiceofferActivity.this, mensaje, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void showNameError(String error) { tilname.setError(error);}

    @Override
    public void showEspecialidadError(String error) { tilespecialidad.setError(error); }

    @Override
    public void showDescripcionError(String error) {tildescripcion.setError(error);}

    @Override
    public void showLocationError(String error) {tillocaltion.setError(error);}

    @Override
    public void showCostoError(String error) { tilcosto.setError(error);}
}
