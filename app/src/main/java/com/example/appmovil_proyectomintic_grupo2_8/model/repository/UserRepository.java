package com.example.appmovil_proyectomintic_grupo2_8.model.repository;

import android.content.Context;
import android.content.Intent;
import android.provider.ContactsContract;
import android.util.Log;

import com.example.appmovil_proyectomintic_grupo2_8.model.database.ServiciosDatabase;
import com.example.appmovil_proyectomintic_grupo2_8.model.database.dao.UserDao;
import com.example.appmovil_proyectomintic_grupo2_8.model.database.entity.User;
import com.example.appmovil_proyectomintic_grupo2_8.mvp.RegistroMVP;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

public class UserRepository {

    private final UserDao userDao;
    private final DatabaseReference userReference;

    private final Boolean savedeviceBD = false;

    public UserRepository(Context context) {
        this.userDao = ServiciosDatabase.getInstance(context).getUserDao();

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        userReference = database.getReference("Usuarios");

        //loadInitialUsers();
        // pruebaFireBase();

    }

    private void pruebaFireBase (){
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("message");
        myRef.setValue("Bienvenidos Servisoluciones");
    }


    // Cargar informacion de usuarios inicial
    private void loadInitialUsers() {

        if (savedeviceBD) {

            // Guardar en Room Database memoria interna

            userDao.insert(new User("User Test","Pererira","Calle 1","311 111 22 33","t@g.com", "1111"));
        } else {
            // Guardar en Firebase

            //User user = (new User("Robinson Bustamante","Calle 1","311 111 22 33","rb@gmail.com","1234"));
            //userReference.child(getEmailId(user.getEmail())).setValue(user);

            User user= (new User("test","Buga","Carrera 1","311 444 55 66","pp@gmail.com","1111"));
            userReference.child(getEmailId(user.getEmail())).setValue(user);
        }
    }


    // Leer usuarios por email
    public void getUserByEmail(String email, GetUserByEmailCallback  callback) {
        if (savedeviceBD) {
            callback.onSuccess(userDao.getUserByEmail(email));
        } else {

            userReference.child(getEmailId(email)).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    // This method is called once with the initial value and again
                    // whenever data at this location is updated.
                    User user = dataSnapshot.getValue(User.class);
                    callback.onSuccess(user);
                }

                @Override
                public void onCancelled(DatabaseError error) {
                    // Error al leer el valor
                    Log.w(UserRepository.class.getSimpleName(), "Error al leer el valor.", error.toException());
                }
            });
        }
        }

        // Convierte el valor de email para usar en BD
        private String getEmailId (String email){
            return email.replace('@', '_').replace('.', '_');
        }

        public interface GetUserByEmailCallback {
            void onSuccess(User user);

            void onFailure();
        }

    // crear usuario
    public void createUser(RegistroMVP.RegistroInfo user, UserCallback<Void> callback){

            Map<String, Object> datosUser = new HashMap<>();
            datosUser.put("Name",user.getName());
            datosUser.put("City",user.getCity());
            datosUser.put("Adress",user.getAdress());
            datosUser.put("Phone",user.getPhone());
            datosUser.put("email",user.getEmail());
            datosUser.put("password",null);


            userReference.child(getEmailId(user.getEmail())).setValue(user);
            //userReference.child("Usuarios").push().setValue(datosUser);

            callback.onSuccess(null);

    }
    public static interface UserCallback<T> {
        void onSuccess(T node);

        void onFailure();
    }


}

