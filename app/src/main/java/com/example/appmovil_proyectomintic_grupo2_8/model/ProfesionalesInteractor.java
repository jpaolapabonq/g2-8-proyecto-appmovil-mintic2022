package com.example.appmovil_proyectomintic_grupo2_8.model;

import com.example.appmovil_proyectomintic_grupo2_8.mvp.ProfesionalesMVP;

import java.util.Arrays;
import java.util.List;

public class ProfesionalesInteractor implements ProfesionalesMVP.Model {

    private List<ProfesionalesMVP.ProfesionalDto> profesionales;

    public ProfesionalesInteractor() {
        profesionales = Arrays.asList(
                new ProfesionalesMVP.ProfesionalDto(
                        "Robinson Bustamante",
                        "311 122 22 22",
                        "rb@gmail.com",
                        "Calle 7 # 10 - 50, Cali, Colombia",
                        "4,5",
                        "$20.000 /Día"
                ),
                new ProfesionalesMVP.ProfesionalDto(
                        "Luis Sanches",
                        "311 101 10 10",
                        "jo@gmail.com",
                        "Calle 7 # 18 - 29, Popayan, Colombia",
                        "4.5",
                        "$25.000 /Día"

                ),
                new ProfesionalesMVP.ProfesionalDto(
                        "Paola Pabon",
                        "311 545 55 55",
                        "pp@gmail.com",
                        "Cra 7 # 18 - 25, Pasto, Colombia",
                        "4.4",
                        "$20.000 /Día"
                )

        );

    }

    @Override
    public void loadProfesionales(LoadProfesionalesCallback callback) {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        callback.setProfesionales(profesionales);
    }
}
