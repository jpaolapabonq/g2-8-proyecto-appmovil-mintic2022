package com.example.appmovil_proyectomintic_grupo2_8.model.repository;


import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import com.example.appmovil_proyectomintic_grupo2_8.model.database.entity.Profesional;


public class ProfesionalesRepository {

    private final DatabaseReference userRef;


    public ProfesionalesRepository() {

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        userRef = database.getReference("Profesionales");


    }
}

    /*private void crearProfesionales(){

        Profesional userRegister = (new Profesional("test user","Calle 1 No 18-25","Bogota","3116418818","t@gmail.com","1111"));
        userRef.child(getEmailId(userRegister.getEmail())).setValue(userRegister);
    }
    // Convierte el valor de email para usar en BD
    private String getEmailId (String email){
        return email.replace('@', '_').replace('.', '_');
    }

    public interface GetUserByEmailCallback {
        void onSuccess(Profesional userRegister);

        void onFailure();
    }

}
*/
