package com.example.appmovil_proyectomintic_grupo2_8.model;

import android.content.Context;

import com.example.appmovil_proyectomintic_grupo2_8.model.database.entity.User;
import com.example.appmovil_proyectomintic_grupo2_8.model.repository.FirebaseAuthRepository;
import com.example.appmovil_proyectomintic_grupo2_8.model.repository.UserRepository;
import com.example.appmovil_proyectomintic_grupo2_8.mvp.LoginMVP;
import com.example.appmovil_proyectomintic_grupo2_8.mvp.RegistroMVP;

public class RegistroInteractor implements RegistroMVP.Model {

    private UserRepository userRepository;
    private final FirebaseAuthRepository firebaseAuthRepository;

    public RegistroInteractor(Context context) {
        userRepository = new UserRepository(context);
        firebaseAuthRepository = FirebaseAuthRepository.getInstance(context);
    }

    @Override
    public void saveRegister(RegistroMVP.RegistroInfo user, saveRegisterCallback callback) {
        firebaseAuthRepository.createUser(user.getEmail(), user.getPassword(),
                new FirebaseAuthRepository.FirebaseAuthCallback() {
                    @Override
                    public void onSuccess() {
                        //user.setUid(firebaseAuthRepository.getCurrentUser().getUid());
                        //user.setPassword(null);
                        userRepository.createUser(user, new UserRepository.UserCallback<Void>() {
                            @Override
                            public void onSuccess(Void node) {
                                callback.onSuccess();
                            }

                            @Override
                            public void onFailure() {
                                //Log.e("fallo","fallo ");
                                callback.onFailure();
                            }
                        });
                    }

                    @Override
                    public void onFailure() {
                        // Enviar error a presenter
                    }
                });
    }


}

