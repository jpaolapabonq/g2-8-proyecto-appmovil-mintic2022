package com.example.appmovil_proyectomintic_grupo2_8.model;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.example.appmovil_proyectomintic_grupo2_8.model.repository.FirebaseAuthRepository;
import com.example.appmovil_proyectomintic_grupo2_8.model.repository.UserRepository;
import com.example.appmovil_proyectomintic_grupo2_8.mvp.LoginMVP;



public class LoginInteractor implements LoginMVP.Model {

    private final UserRepository userRepository;
    private final FirebaseAuthRepository firebaseAuthRepository;

    public LoginInteractor(Context context) {
        userRepository = new UserRepository(context);
        firebaseAuthRepository = FirebaseAuthRepository.getInstance(context);
        }

    @Override
    public void validateCredentials(String email, String password,
            ValidateCredentialsCallback callback) {

        firebaseAuthRepository.validateUser(email, password,
                new FirebaseAuthRepository.FirebaseAuthCallback() {
            @Override
            public void onSuccess() {
                //Log.i("creado", "regsitro exitoso");
                callback.onSuccess();
            }

            @Override
            public void onFailure() {
                //Log.e("fallo","fallo ");
                callback.onFailure();
            }
        });
    }

    @Override
    public boolean hasAuthenticatedUser() {
        return firebaseAuthRepository.isAuthenticated();
    }

    @Override
    public Intent getGoogleIntent() {return firebaseAuthRepository.getGoogleSingInIntent();
    }


    @Override
    public void setGoogleData(Intent data, ValidateCredentialsCallback callback) {
        firebaseAuthRepository.setGoogleData(data,
                new FirebaseAuthRepository.FirebaseAuthCallback() {
                    @Override
                    public void onSuccess() {
                        callback.onSuccess();
                    }

                    @Override
                    public void onFailure() {
                        callback.onFailure();
                    }
                });
    }

}

        // try { Thread.sleep(5000);} catch (InterruptedException e) { e.printStackTrace();}
        /* userRepository.getUserByEmail(email, new UserRepository.GetUserByEmailCallback(){
            @Override
            public void onSuccess(User user) {
                if(user != null && user.getPassword() .equals(password)){
                    callback.onSuccess();
                } else {
                    callback.onFailure();
                }
            }
            @Override
            public void onFailure() {
                callback.onFailure();
            }
        });
        */
