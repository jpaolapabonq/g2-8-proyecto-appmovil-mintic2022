package com.example.appmovil_proyectomintic_grupo2_8.model.database.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Profesional {

    @PrimaryKey(autoGenerate = true)
    private Integer id;

    // Atributos - Columnas en tabla UserR
    private String name;
    private String adress;
    private String city;
    private String phone;
    private String email;
    private String especialidad;
    private Double calificacion;


    // Constuctores


    public Profesional() {
    }
    // Informacion que se solicita al usuario en formulario

    public Profesional(String name, String adress, String city, String phone, String email, String especialidad, Double calificacion) {
        this.name = name;
        this.adress = adress;
        this.city = city;
        this.phone = phone;
        this.email = email;
        this.especialidad = especialidad;
        this.calificacion = calificacion;
    }


    // Setter

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    public void setCalificacion(Double calificacion) {
        this.calificacion = calificacion;
    }

    // Getter


    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAdress() {
        return adress;
    }

    public String getCity() {
        return city;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public Double getCalificacion() {
        return calificacion;
    }
}
