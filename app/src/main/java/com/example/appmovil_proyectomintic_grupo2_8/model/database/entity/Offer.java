package com.example.appmovil_proyectomintic_grupo2_8.model.database.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Offer {

    @PrimaryKey(autoGenerate = true)
    private Integer id;

    // Atributos - Columnas en tabla UserR
    private String name;
    private String especialidad;
    private String descripcion;
    private String location;
    private String costo;
    private Double calificacion;
    private Boolean enable;


    // Constuctores


    public Offer() {
    }
    // Informacion que se solicita al usuario en formulario

    public Offer(String name, String especialidad, String descripcion, String location, String costo) {

        this.name = name;
        this.especialidad = especialidad;
        this.descripcion = descripcion;
        this.location = location;
        this.costo = costo;
    }


    // Setter

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setCosto(String costo) {
        this.costo = costo;
    }

    public void setCalificacion(Double calificacion) {
        this.calificacion = calificacion;
    }


// Getter


    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getLocation() {
        return location;
    }

    public String getCosto() {
        return costo;
    }

    public Double getCalificacion() {
        return calificacion;
    }


}
