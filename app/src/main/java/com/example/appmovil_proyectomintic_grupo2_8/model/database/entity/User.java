package com.example.appmovil_proyectomintic_grupo2_8.model.database.entity;


import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class User {

    @PrimaryKey(autoGenerate = true)
    private Integer id;

    // Atributos
    // Columnas en la base de datos
    private String Uid;
    private String name;
    private String city;
    private String adress;
    private String phone;
    private String email;
    private String password;


    //CONSTRUCTORES

    public User() {
    // Vacio para el uso de Room
    }
    // Informacion que se solicita al usuario



    public User(String name, String city, String adress, String phone, String email, String password) {
        this.name = name;
        this.city = city;
        this.adress = adress;
        this.phone = phone;
        this.email = email;
        this.password = password;

    }



    // SETTER

    public void setId(Integer id) {this.id = id;}

    public void setName(String name) {this.name = name;}

    public void setAdress(String adress) {this.adress = adress;}

    public void setPhone(String phone) {this.phone = phone; }

    public void setEmail(String email) {this.email = email; }

    public void setPassword(String password) {this.password = password; }

    public void setUid(String uid) {
        Uid = uid;
    }

    public void setCity(String city) {this.city = city;
    }
//GETTER


    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAdress() {
        return adress;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getCity() {return city;
    }

    public String getUid() {
        return Uid;
    }
}
