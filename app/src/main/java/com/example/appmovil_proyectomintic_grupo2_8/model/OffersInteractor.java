package com.example.appmovil_proyectomintic_grupo2_8.model;

import android.content.Context;
import android.util.Log;

import com.example.appmovil_proyectomintic_grupo2_8.model.repository.OfferRepository;
import com.example.appmovil_proyectomintic_grupo2_8.model.repository.UserRepository;
import com.example.appmovil_proyectomintic_grupo2_8.mvp.OffersMVP;

public class OffersInteractor implements OffersMVP.Model {

    private final OfferRepository offerRepository;

    public OffersInteractor(Context context) {
        offerRepository = new OfferRepository(context);
    }

    @Override
    public void saveOffer(OffersMVP.OfferInfo offer, saveOfferCallback callback) {
       offerRepository.createoffer(offer, new OfferRepository.OfferCallback (){

           @Override
           public void onSuccess() {
               callback.onSuccess();
           }

           @Override
           public void onFailure() {
               callback.onFailure();
           }
       });

    }
}


