package com.example.appmovil_proyectomintic_grupo2_8.model.database.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.appmovil_proyectomintic_grupo2_8.model.database.entity.User;

import java.util.List;

@Dao
public interface UserDao {

    @Query("SELECT * FROM user")
    List<User> getAll();

    @Query("SELECT * FROM user WHERE email = :email")
    User getUserByEmail(String email);
    // :email los dos puntos  se utilizan para enlazar el paramentro en la consulta

    @Insert
    void insert(User user);

    @Update
    void update(User user);
    // actualiza toda la tabla permite mapear toda la tabla

    @Delete
    void delete(User user);
}
