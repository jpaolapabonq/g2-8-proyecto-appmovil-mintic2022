package com.example.appmovil_proyectomintic_grupo2_8.model.database;


import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.appmovil_proyectomintic_grupo2_8.model.database.dao.UserDao;
import com.example.appmovil_proyectomintic_grupo2_8.model.database.entity.User;

//Configuracion de entidades
@Database(entities = {User.class}, version = 1)
public abstract class ServiciosDatabase extends RoomDatabase {

    private volatile static ServiciosDatabase instance;

    public static ServiciosDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room
                    .databaseBuilder(context.getApplicationContext(), ServiciosDatabase.class, "servicios-database")
                    .allowMainThreadQueries()
                    .build();
        }
        return instance;
    }

    //configuracion de metodos para obtener los dao
    public abstract UserDao getUserDao();
}
