package com.example.appmovil_proyectomintic_grupo2_8.model.repository;

import com.example.appmovil_proyectomintic_grupo2_8.model.database.entity.Offer;
import com.example.appmovil_proyectomintic_grupo2_8.model.database.entity.User;
import com.example.appmovil_proyectomintic_grupo2_8.mvp.OffersMVP;
import com.example.appmovil_proyectomintic_grupo2_8.mvp.RegistroMVP;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import android.content.Context;
import android.util.Log;

import java.util.HashMap;
import java.util.Map;


public class OfferRepository {

    private FirebaseUser  user;
    private  DatabaseReference offerReference;

    public OfferRepository(Context context) {

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        offerReference = database.getReference();
    }

    // crear usuario
    public void createoffer(OffersMVP.OfferInfo offer, OfferCallback callback) {


        //userReference.child(user.getUid()).setValue(userToSave);
        //offerReference.child(offer.getName()).setValue(offerToSave);

        Map<String, Object> datosoffer = new HashMap<>();
        datosoffer.put("Name", offer.getName());
        datosoffer.put("Especialidad", offer.getEspecialidad());
        datosoffer.put("Descripcion", offer.getDescripcion());
        datosoffer.put("Ubicacion", offer.getLocation());
        datosoffer.put("Costo", offer.getCosto());

        offerReference.child("OfertaServicios").push().setValue(datosoffer);

        callback.onSuccess();

    }

    public interface OfferCallback  {
        void onSuccess();

        void onFailure();
    }

    public void getUserBykey(String key, GetUserByKeyCallback callback) {

          offerReference.child("OfertaServicios").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    // This method is called once with the initial value and again
                    // whenever data at this location is updated.
                    Offer offer = dataSnapshot.getValue(Offer.class);
                    callback.onSuccess(offer);
                }

                @Override
                public void onCancelled(DatabaseError error) {
                    // Error al leer el valor
                    Log.e("fallo", "fallo");
                }
            });
        }
    public interface GetUserByKeyCallback {
        void onSuccess(Offer offer);

        void onFailure();
    }



}
