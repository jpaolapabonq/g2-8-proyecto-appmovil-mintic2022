package com.example.appmovil_proyectomintic_grupo2_8.presenter;

import android.app.ProgressDialog;
import android.util.Log;

import androidx.annotation.NonNull;

import com.example.appmovil_proyectomintic_grupo2_8.model.LoginInteractor;
import com.example.appmovil_proyectomintic_grupo2_8.model.OffersInteractor;
import com.example.appmovil_proyectomintic_grupo2_8.mvp.LoginMVP;
import com.example.appmovil_proyectomintic_grupo2_8.mvp.OffersMVP;
import com.example.appmovil_proyectomintic_grupo2_8.mvp.RegistroMVP;
import com.example.appmovil_proyectomintic_grupo2_8.view.ServiceofferActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.core.Context;

import java.util.HashMap;
import java.util.Map;

public class OffersPresenter implements OffersMVP.Presenter {


    private final OffersMVP.View view;
    private final OffersMVP.Model model;


    public OffersPresenter(OffersMVP.View view) {

        this.view = view;
        this.model = new OffersInteractor(view.getActivity());
    }

    @Override
    public void onSaveOfferClick() {
        boolean error = false;
        OffersMVP.OfferInfo offerInfo = view.getOfferInfo();

        //Valido datos
        view.showGeneralError("");

        view.showNameError("");
        view.showEspecialidadError("");
        view.showDescripcionError("");
        view.showLocationError("");
        view.showCostoError("");

        if (offerInfo.getName().isEmpty()) {
            view.showNameError("Nombre es obligatorio");
            error = true;
        }

        if (offerInfo.getEspecialidad().isEmpty()) {
            view.showEspecialidadError("Este campo es obligatoria");
            error = true;
        }
        if (offerInfo.getLocation().isEmpty()) {
            view.showLocationError("Este campo es obligatoria");
            error = true;
        }
        if (offerInfo.getCosto().isEmpty()) {
            view.showCostoError("Este campo es obligatoria");
            error = true;
        }


        if (!error) {
            //view.showProgresBar();
            //offerReference = FirebaseDatabase.getInstance().getReference();
            new Thread(() -> {
                model.saveOffer(offerInfo,
                        new OffersMVP.Model.saveOfferCallback() {
                            @Override
                            public void onSuccess() {
                                view.getActivity().runOnUiThread(() -> {
                                    //view.hideProgresBar();
                                    view.showGeneralMsg("Registro Exitoso");
                                    view.showPerfilProfesionalActivity();
                                });
                            }

                            @Override
                            public void onFailure() {
                                view.getActivity().runOnUiThread(() -> {
                                    //view.hideProgresBar();
                                    view.showGeneralError("Fallo registro");
                                });
                            }
                        });
            }).start();
        }

    }

}
