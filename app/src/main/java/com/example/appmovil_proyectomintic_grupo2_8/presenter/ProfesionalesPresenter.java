package com.example.appmovil_proyectomintic_grupo2_8.presenter;

import android.os.Bundle;

import com.example.appmovil_proyectomintic_grupo2_8.model.ProfesionalesInteractor;
import com.example.appmovil_proyectomintic_grupo2_8.mvp.ProfesionalesMVP;

public class ProfesionalesPresenter implements ProfesionalesMVP.Presenter {

    private ProfesionalesMVP.View view;
    private ProfesionalesMVP.Model model;

    public ProfesionalesPresenter(ProfesionalesMVP.View view) {
        this.view = view;
        this.model =  new ProfesionalesInteractor();
    }

    @Override
    public void loadProfesionales() {
        view.showProgressBar();
        new Thread(() -> {
            model.loadProfesionales(profesionales -> view.getActivity().runOnUiThread(() -> {
                view.hideProgressBar();
                view.showProfesioanles(profesionales);
            }));

        }).start();
    }

    @Override
    public void onProfesionalClick() {

    }

    @Override
    public void onLlamarClick() {

    }

    @Override
    public void onCotizacionClick() {

    }

    @Override
    public void onLocalizacion() {

    }

    @Override
    public void onItemSelected(ProfesionalesMVP.ProfesionalDto info) {
        Bundle params = new Bundle();
        params.putString("name",info.getNameprofesional());
        params.putString("address", info.getLocationprofesional());

        view.openLocationActivity(params);
    }
}

