package com.example.appmovil_proyectomintic_grupo2_8.presenter;

import com.example.appmovil_proyectomintic_grupo2_8.model.EvaluacionInteractor;
import com.example.appmovil_proyectomintic_grupo2_8.mvp.EvaluacionMVP;

public class EvaluacionPresenter implements EvaluacionMVP.Presenter {
    private EvaluacionMVP.View view;
    private EvaluacionInteractor model;

    public EvaluacionPresenter (EvaluacionMVP.View view){
        this.view = view;
        this.model = new EvaluacionInteractor();
    }

    @Override
    public void onEnviarCalificacionClick() {

    }

    @Override
    public void onRegisterClick() {

    }
}

