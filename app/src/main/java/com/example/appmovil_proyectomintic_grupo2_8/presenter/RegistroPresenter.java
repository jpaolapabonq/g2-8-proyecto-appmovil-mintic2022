package com.example.appmovil_proyectomintic_grupo2_8.presenter;

import com.example.appmovil_proyectomintic_grupo2_8.model.LoginInteractor;
import com.example.appmovil_proyectomintic_grupo2_8.model.RegistroInteractor;
import com.example.appmovil_proyectomintic_grupo2_8.mvp.LoginMVP;
import com.example.appmovil_proyectomintic_grupo2_8.mvp.RegistroMVP;

public class RegistroPresenter implements RegistroMVP.Presenter {
    private RegistroMVP.View view;
    private RegistroMVP.Model model;

    public RegistroPresenter (RegistroMVP.View view){
        this.view = view;
        this.model = new RegistroInteractor(view.getActivity());
    }


    @Override
    public void onRegisterClick() {
        boolean error = false;

        RegistroMVP.RegistroInfo registroInfo = view.getRegistroInfo();

        //Validación de datos:
        view.showEmailError("");
        view.showPasswordError("");
        view.showNameError("");
        view.showPhoneError("");
        view.showAdressError("");

        if (registroInfo.getName().isEmpty()) {
            view.showNameError("Este campo es obligatorio");
            error = true;
        }

        if (registroInfo.getAdress().isEmpty()) {
            view.showAdressError("Direccion es obligatorio");
            error = true;
        }

        if (registroInfo.getPhone().isEmpty()) {
            view.showPhoneError("Numero de telefono es obligatorio");
            error = true;
        } else if (!isPhoneValid(registroInfo.getPhone())) {
            view.showPhoneError("Numero de telefono no es valido");
            error = true;
        }

        if (registroInfo.getEmail().isEmpty()) {
            view.showEmailError("Correo electrónico es obligatorio");
            error = true;
        } else if (!isEmailValid(registroInfo.getEmail())) {
            view.showEmailError("Correo electrónico no es válido");
            error = true;
        }
        if (registroInfo.getPassword().isEmpty()) {
            view.showPasswordError("Contraseña es obligatoria");
            error = true;
        } else if (!isPasswordValid(registroInfo.getPassword())) {
            view.showPasswordError("Contraseña no cumple criterios de seguridad");
            error = true;
        }

        if (!error) {
                //view.showProgresBar();
                new Thread(() -> {
                    model.saveRegister (registroInfo,
                            new RegistroMVP.Model.saveRegisterCallback() {
                                @Override
                                public void onSuccess() {
                                    view.getActivity().runOnUiThread(() -> {
                                        //view.hideProgresBar();
                                        view.showGeneralError("Registro Exitoso");
                                        view.showLoginActivity();
                                    });
                                }

                                @Override
                                public void onFailure() {
                                    view.getActivity().runOnUiThread(() -> {
                                        //view.hideProgresBar();
                                        view.showGeneralError("Informacion no válida");
                                    });
                                }
                            });
                }).start();
            }
    }

    private boolean isEmailValid(String email) {
        return email.contains("@")
                && email.endsWith(".com");
    }


    private boolean isPasswordValid(String password) { return password.length() >= 4;
    }
    private boolean isPhoneValid(String phone) {
        return phone.length() >= 10;
    }


}
